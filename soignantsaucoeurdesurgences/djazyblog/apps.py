from django.apps import AppConfig


class DjazyblogConfig(AppConfig):
    name = 'djazyblog'
