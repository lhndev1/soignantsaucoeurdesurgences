from django.core.mail import send_mail
#from django.contrib.postgres.search import SearchVector, SearchQuery, SearchRank
from django.contrib.postgres.search import TrigramSimilarity
from django.db.models.functions import Greatest
from django.db.models import Count
from django.urls import reverse, reverse_lazy
from django.views import View
from django.views.generic import ListView, DetailView, FormView
from django.views.generic.detail import SingleObjectMixin
from django.shortcuts import render, get_object_or_404
#from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from taggit.models import Tag
from .models import Post
from .forms import EmailPostForm, CommentForm, SearchForm


class PostListView(ListView):
    queryset = Post.published.all()
    context_object_name = 'posts'
    paginate_by = 3
    template_name = 'blog/post/list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.kwargs.get('tag_slug'):
            tag = get_object_or_404(Tag, slug=self.kwargs.get('tag_slug'))
            context['tag'] = tag
            context['posts'] = self.queryset.filter(tags__in=[tag])
        return context


# Display the Post and its 'comments with form'  
class PostDisplay(DetailView):
    model = Post
    context_object_name = 'post'
    template_name = 'blog/post/detail.html'

    def get_object(self):
        return get_object_or_404(
                self.model,
                slug=self.kwargs['slug'],
                status='published',
                publish__year=self.kwargs['year'],
                publish__month=self.kwargs['month'],
                publish__day=self.kwargs['day']
                )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        post = context['post']
        # List of similar posts
        post_tags_ids = post.tags.values_list('id', flat=True)
        similar_posts = Post.published.filter(
                tags__in=post_tags_ids
                ).exclude(id=post.id)

        context['similar_posts'] = similar_posts.annotate(
                same_tags=Count('tags')
                ).order_by('-same_tags','-publish')[:4]
        
        context['comment_form'] = CommentForm()
        context['comments'] = post.comments.filter(active=True)
        
        if self.kwargs.get('success'):
            context['sent'] = True
        
        return context


class PostComments(SingleObjectMixin, FormView):
    template_name = 'blog/post/detail.html'
    form_class = CommentForm
    model = Post

    def post(self, request, *args, **kwargs):
        self.object = get_object_or_404(
                self.model,
                slug=self.kwargs['slug'],
                status='published',
                publish__year=self.kwargs['year'],
                publish__month=self.kwargs['month'],
                publish__day=self.kwargs['day']
                )

        self.comment_form = CommentForm(data=request.POST)
        if self.comment_form.is_valid():
            # Create Comment object but don't save to database yet
            self.new_comment = self.comment_form.save(commit=False)
            # Assign the current post to the comment
            self.new_comment.post = self.object
            # Save the comment to the database
            self.new_comment.save()
        else:
            self.comment_form = CommentForm()

        return super().post(request, *args, **kwargs)

    def get_success_url(self, **kwargs):
        return reverse(
                'blog:post_detail_com_sent',
                kwargs= {
                    'year': self.object.publish.strftime('%Y'),
                    'month': int(self.object.publish.strftime('%m')),
                    'day': int(self.object.publish.strftime('%d')),
                    'slug': self.object.slug,
                    'success': 'success'
                    }
                )


class PostDetailView(View):

    def get(self, request, *args, **kwargs):
        view = PostDisplay.as_view()
        return view(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        view = PostComments.as_view()
        return view(request, *args, **kwargs)
# End 


class PostShareView(FormView):
    
    template_name = 'blog/post/share.html'
    form_class = EmailPostForm

    def get_success_url(self, **kwargs):
        return reverse("blog:post_share_success", 
                kwargs={'post_id': self.kwargs.get('post_id'), 'success': 'success'}
                )

    def form_valid(self, form):
        post = get_object_or_404(
                Post, 
                id=self.kwargs.get('post_id'),
                status='published'
                )
        post_url = self.request.build_absolute_uri(
                post.get_absolute_url()
                )
        subject = f"{form.cleaned_data['name']} recommends you read " \
                f"{post.title}"
        message = f"Read {post.title} at {post_url}\n\n" \
                f"{form.cleaned_data['name']}\'s comments: {form.cleaned_data['comments']}"
        send_mail(subject, message, 'djazyblog@lhndev.com', [form.cleaned_data['to']])
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(PostShareView, self).get_context_data(**kwargs)
        context['post'] = Post.objects.get(id=self.kwargs.get('post_id')
                # get_object_or_404(
                # Post, 
                # id=self.kwargs.get('post_id'),
                # status='published'
                )
        if self.kwargs.get('success'):
            context['sent'] = True
        return context


def post_search(request):
    form = SearchForm()
    query = None
    results = []
    if 'query' in request.GET:
        form = SearchForm(request.GET)
        if form.is_valid():
            query = form.cleaned_data['query']
            # Basic Search in the title & body fiels
            # results = Post.published.annotate(
                    # search=SearchVector('title', 'body'),
                    # ).filter(search=query)

            # Stemming and ranking search results
            # search_vector = SearchVector('title', 'body')
            # search_query = SearchQuery(query)
            # results = Post.published.annotate(
                    # search=search_vector,
                    # rank=SearchRank(search_vector, search_query)
                    # ).filter(search=search_query).order_by('-rank')

            # Stemming and ranking results with weighting queries
            # search_vector = SearchVector('title', weight='A') + \
                            # SearchVector('body', weight='B')
            # search_query = SearchQuery(query)
            # results = Post.published.annotate(
                    # search=search_vector,
                    # rank=SearchRank(search_vector, search_query)
                    # ).filter(rank__gte=0.3).order_by('-rank')

            # Searching with trigram similarity
            results = Post.published.annotate(
                    similarity=TrigramSimilarity('title', query),
                    ).filter(similarity__gte=0.1).order_by('-similarity')

            # Searching with trigram similarity in two fiel using Greatest django db function 
            results = Post.published.annotate(
                    similarity=Greatest(
                        TrigramSimilarity('title', query),
                        TrigramSimilarity('body', query)
                    )).filter(similarity__gte=0.1).order_by('-similarity')

    return render(request,
            'blog/post/search.html',
            {
                'form': form,
                'query': query,
                'results': results
                }
            )
