from django.urls import path
from .views import PostDetailView, PostListView, PostShareView, post_search
from .feeds import LatestPostsFeed


app_name = 'blog'


urlpatterns = [
        path('', PostListView.as_view(), name='post_list'),
        path('tag/<slug:tag_slug>/', PostListView.as_view(), name='post_list_by_tag'),

        path(
            '<int:year>/<int:month>/<int:day>/<slug:slug>/', PostDetailView.as_view(), name='post_detail'),
        path(
            '<int:year>/<int:month>/<int:day>/<slug:slug>',
            PostDetailView.as_view(),
            {'success': 'success'},
            name='post_detail_com_sent'),

        path('<int:post_id>/share/', PostShareView.as_view(), name='post_share'),
        path(
            '<int:post_id>/share',
            PostShareView.as_view(),
            {'success': 'success'},
            name='post_share_success'),

        path('feed/', LatestPostsFeed(), name='post_feed'),
        path('search/', post_search, name='post_search'),
        ]

