from .base import *

# Quick-start development settings - unsuitable for production

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True 

ALLOWED_HOSTS = ['127.0.0.1', 'localhost', '192.168.0.33']
INTERNAL_IPS = '127.0.0.1'

INSTALLED_APPS += [
    #Debug
    'debug_toolbar',
]

SITE_ID = 1

MIDDLEWARE += [
    #Debug
    'debug_toolbar.middleware.DebugToolbarMiddleware',
]

# Database
# DATABASES = {
    # 'default': {
        # 'ENGINE': 'django.db.backends.sqlite3',
        # 'NAME': BASE_DIR.parent / 'db.sqlite3',
    # }
# }
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': get_env_variable('DB_NAME'),
        'USER': get_env_variable('DB_USER'),
        'PASSWORD': get_env_variable('DB_PASSWORD'),
        'HOST': 'localhost',
        'PORT': '',
    }
}
EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"

# Internationalization
# https://docs.djangoproject.com/en/3.1/topics/i18n/

#LANGUAGE_CODE = 'en-us'
LANGUAGE_CODE = 'fr-fr'


#TIME_ZONE = 'UTC'
TIME_ZONE =  'Europe/Paris'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# LANGUAGES = (
    # ('en', _('English')),
    # ('fr', _('French')),
# )

# LOCALE_PATHS = (BASE_DIR / 'locale',)


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.1/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = BASE_DIR / 'static'

#STATICFILES_DIRS = [BASE_DIR /"static",]

