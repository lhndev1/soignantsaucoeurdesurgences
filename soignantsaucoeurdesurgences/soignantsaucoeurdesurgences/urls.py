"""soignantsaucoeurdesurgences URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.contrib import admin
from django.contrib.sitemaps.views import sitemap
from django.urls import include, path
from djazyblog.sitemaps import PostSitemap


admin.site.site_header = 'Administration du site Soignant Au Coeur Des Urgences'
admin.site.site_title = "Panneau d'administration du site Soignant Au Coeur Des Urgences"
admin.site.index_title = "Bienvenue sur le panneau d'administration du site Soignant Au Coeur Des Urgences"


sitemaps = {
        'posts': PostSitemap,
        }

urlpatterns = [
    # path('', Home.as_view(), name='home'),
    path('grappelli/', include('grappelli.urls')),
    path('', include('djazyblog.urls', namespace='blog')),
    path('admin/', admin.site.urls),
    path('sitemap.xml', sitemap, {'sitemaps': sitemaps},
        name='django.contrib.sitemaps.views.sitemap')
]

if settings.DEBUG:  # pragma: no cover
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns

